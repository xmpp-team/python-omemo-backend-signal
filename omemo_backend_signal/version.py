__version__ = {}
__version__["short"] = "0.3.0"
__version__["tag"]   = "beta"
__version__["full"]  = "{}-{}".format(__version__["short"], __version__["tag"])
